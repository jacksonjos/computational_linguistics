#!/usr/bin/python
from morphosyntactic_training import MorphosyntacticTraining
import sys
import os
import cProfile
import statistics
import ujson

import cpuinfo
import psutil
import platform
import math
import random


def get_seeds(sentences_statistics_dirpath):
    """
    """
    seeds_filepath = os.path.join(sentences_statistics_dirpath, "seeds.json")
    if not os.path.exists(seeds_filepath):
        seeds = [random.randint(0, 1000) for i in range(10)]
        with open(seeds_filepath, 'w') as file:
            ujson.dump(seeds, file, indent=4)
    else:
        with open(seeds_filepath, 'r') as file:
            seeds = ujson.load(file)

    return seeds

def extract_corpus(corpus_filepath, sentences_statistics_dirpath):
    """

    """
    morpho_training = MorphosyntacticTraining()
    morpho_training.extract_sentences(corpus_filepath)
    morpho_training.write_sentences_and_statistics(sentences_statistics_dirpath)
    morpho_training.calculate_corpus_baseline_accuracy()

def tag_by_counting(morpho_training, seeds, i):
    """
    """
    morpho_training.separate_corpus(seeds[i])
    morpho_training.build_training_counts()
    morpho_training.calc_emission_probs()
    morpho_training.calc_transition_probs()
    viterbi_accuracy = morpho_training.calculate_corpus_viterbi_accuracy()
    morpho_training.free_experiment_variables_memory()

    return viterbi_accuracy

def tagging_by_counting_simulations(sentences_statistics_dirpath):
    """
    """
    morpho_training = MorphosyntacticTraining()
    morpho_training.read_sentences_and_statistics(sentences_statistics_dirpath)
    morpho_training.calculate_corpus_baseline_accuracy()
    seeds = get_seeds(sentences_statistics_dirpath)

    accuracy_lst = [tag_by_counting(morpho_training, seeds, i) for i in range(10)]

    mean_accuracy = statistics.mean(accuracy_lst)
    stdev_accuracy = statistics.stdev(accuracy_lst)
    min_accuracy = min(accuracy_lst)
    max_accuracy = max(accuracy_lst)

    print("The mean accuracy is: ", mean_accuracy)
    print("The standard deviation is: ", stdev_accuracy)
    print("The min accuracy is: ", min_accuracy)
    print("The max accuracy is: ", max_accuracy)

def profile_activity_execution(func, step_name, *args):
    """
    """
    profiler = cProfile.Profile()
    profiler.runcall(func, *args)
    profiler.dump_stats(step_name + '.prof')


def write_system_info(step_name):
    """
    """
    file = open(step_name + "_machine_config.txt", 'w')

    info = cpuinfo.get_cpu_info()
    file.write("Processors: {}\n".format(info['brand']))
    file.write("Number of cores: {}\n".format(info['count']))
    file.write("L1 cache memory size: {}\n".format(info['l1_data_cache_size']))
    file.write("L2 cache memory size: {}\n".format(info['l2_cache_size']))
    file.write("L3 cache memory size: {}\n".format(info['l3_cache_size']))

    memory_size = round(psutil.virtual_memory().total / math.pow(2, 30))
    file.write("\nPhysical memory size: {} GB\n".format(memory_size))

    os_name = platform.system()
    if os_name == 'Linux':
        os_version = " ".join([s.capitalize() for s in platform.linux_distribution()])
        file.write("Operating system: {} {}\n".format(os_name, os_version.strip()))
        file.write("Kernel: {}\n".format(platform.release()))
    else:
        file.write("Operating system: {} {}".format(os_name, platform.release()))

    file.close()


if __name__ == '__main__':
    if sys.argv[1] == 'extract_corpus':
        profile_activity_execution(extract_corpus, *sys.argv[1:])
    elif sys.argv[1] == 'tagging_by_counting_simulations':
        profile_activity_execution(tagging_by_counting_simulations, *sys.argv[1:])

    write_system_info(sys.argv[1])
