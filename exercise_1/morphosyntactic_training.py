#!/usr/bin/python
import collections
import random
import ujson
import re
import os
from tqdm import tqdm
import numpy as np

MIN_VALUE = float('-inf')


class MorphosyntacticTraining:
    """Class for morphosyntactic tags training using Hidden Markov Model."""

    def __init__(self):
        self.pos_counts_dict = collections.defaultdict(int)
        self.token_pos_counts_dict = collections.defaultdict(lambda: collections.defaultdict(int))
        self.sentences = []


    def get_pair_token_pos(self, pair_data):
        """
            This function extracts the pair token and part of speech from the string 'pair'
            and returns it like a tuple.
        """
        try:
            # token: begins with a number or letter and ends begin the first space character
            token = re.search(r'^(\w[^\s]+)?\w', pair_data, re.UNICODE).group()
            # This pattern supposes that the patter begins with a world class tag
            pos_tag = re.search(r'\s[A-Z_]+\s', pair_data, re.UNICODE).group().strip()
            # guaranteeing that extract tag is not invalid
            assert pos_tag not in ["GER", "PP", "PRP_DET", "PRON"]
        except:
            return None

        return {'token': token, 'pos_tag': pos_tag}

    def begins_with_token(self, line):
        """
        """
        return re.search(r'^\w[^ ]+', line, re.UNICODE)

    def increment_statistics(self, lexical_pair):
        """
        """
        self.pos_counts_dict[lexical_pair['pos_tag']] +=  1
        self.token_pos_counts_dict[lexical_pair['token']][lexical_pair['pos_tag']] += 1

    def extract_sentences(self, corpus_filepath):
        """
        """
        sentence = []
        sentence_started = False
        print("Extracting sentences.")
        fp = open("issues.log", 'w')
        with open(corpus_filepath) as file:
            for i, line in enumerate(tqdm(file, total=35236585)):
                if sentence_started and self.begins_with_token(line):
                    lexical_pair = self.get_pair_token_pos(line)
                    if lexical_pair:
                        self.increment_statistics(lexical_pair)
                        sentence.append(lexical_pair)
                    else:
                        fp.write(line)
                elif line.startswith('<s>'):
                    sentence_started = True
                elif sentence_started and line.startswith('</s>'):
                    self.sentences.append(sentence)
                    sentence_started = False
                    sentence = []
        fp.close()

    def write_sentences_and_statistics(self, output_dirpath):
        """
        """
        print("Writing sentences and other data structures.")
        if not os.path.exists(output_dirpath):
            os.makedirs(output_dirpath)

        sentences_filepath = os.path.join(output_dirpath, 'sentences.json')
        with open(sentences_filepath, 'w') as file:
            ujson.dump(self.sentences, file, indent=4)

        pos_count_filepath = os.path.join(output_dirpath, 'pos_count.json')
        with open(pos_count_filepath, 'w') as file:
            ujson.dump(self.pos_counts_dict, file, indent=4)

        token_pos_count_filepath = os.path.join(output_dirpath, 'token_pos_count.json')
        with open(token_pos_count_filepath, 'w') as file:
            ujson.dump(self.token_pos_counts_dict, file, indent=4)

    def read_sentences_and_statistics(self, input_dirpath):
        """
        """
        print("Reading sentences and other data structures.")
        sentences_filepath = os.path.join(input_dirpath, 'sentences.json')
        with open(sentences_filepath, 'r') as file:
            self.sentences = ujson.load(file)

        pos_count_filepath = os.path.join(input_dirpath, 'pos_count.json')
        with open(pos_count_filepath, 'r') as file:
            self.pos_counts_dict = ujson.load(file)

        token_pos_count_filepath = os.path.join(input_dirpath, 'token_pos_count.json')
        with open(token_pos_count_filepath, 'r') as file:
            self.token_pos_counts_dict = ujson.load(file)

    def get_most_frequent_pos_tags(self):
        """
        """
        self.token_pos_top_tag_dict = {pos_counts[0]: sorted(pos_counts[1].items(), key=lambda d: d[1], reverse=True)[0][0]
                                                        for pos_counts in self.token_pos_counts_dict.items()}

    def calculate_corpus_baseline_accuracy(self):
        """
        """
        print("Calculating baseline accuracy.")
        self.get_most_frequent_pos_tags()
        tagging_check_answer_lst = [self.token_pos_top_tag_dict[lexical_pair['token']] == lexical_pair['pos_tag']
                                    for sentence in tqdm(self.sentences, total=len(self.sentences))
                                    for lexical_pair in sentence]

        correct_tags = sum(tagging_check_answer_lst)

        baseline_acuracy = correct_tags / len(tagging_check_answer_lst)
        print("Baseline accuracy: ", baseline_acuracy)

        self.filter_sentences()
        tagging_check_answer_min_3_words_lst = [self.token_pos_top_tag_dict[lexical_pair['token']] == lexical_pair['pos_tag']
                                    for sentence in tqdm(self.analyzed_sentences, total=len(self.analyzed_sentences))
                                    for lexical_pair in sentence]

        correct_tags_min_3_words = sum(tagging_check_answer_min_3_words_lst)

        baseline_acuracy = correct_tags_min_3_words / len(tagging_check_answer_min_3_words_lst)
        print("Baseline accuracy for at least 3 words: ", baseline_acuracy)


    def filter_sentences(self):
        """
        """
        if not hasattr(self, 'analyzed_sentences'):
            n = len(self.sentences)
            print("There are {} sentences.".format(n))
            self.analyzed_sentences = [sentence for sentence in self.sentences
                                        if len(sentence) > 2]
            m = len(self.analyzed_sentences)
            print("There are {} sentences with at least 3 words.".format(m))


    def separate_corpus(self, seed):
        """
        """
        print("Separating corpus between training and test sets.")
        self.filter_sentences()
        m = len(self.analyzed_sentences)
        training_set_size = round(m * 0.8)
        random.seed(seed)
        shuffled_sentences = random.sample(self.analyzed_sentences, k=m)
        self.training_set_lst = shuffled_sentences[:training_set_size]
        self.test_set_lst = shuffled_sentences[training_set_size:]

    def build_training_counts(self):
        """
        """
        print("Building training counts.")
        self.tr_pos_counts_dict = collections.defaultdict(int)
        self.tr_token_pos_counts_dict = collections.defaultdict(lambda: collections.defaultdict(int))
        for sentence in tqdm(self.training_set_lst, total=len(self.training_set_lst)):
            for lexical_pair in sentence:
                self.tr_pos_counts_dict[lexical_pair['pos_tag']] +=  1
                self.tr_token_pos_counts_dict[lexical_pair['token']][lexical_pair['pos_tag']] += 1

    def calc_emission_probs(self):
        """
        """
        print("Calculating emission probabilities.")
        # counting word for each tag
        self.word_tag_prob = {tag: {word: word_freq[tag] + 1
                                for word, word_freq in self.tr_token_pos_counts_dict.items() if tag in word_freq}
                                for tag, count in self.tr_pos_counts_dict.items()
                             }
        # each word that has a tag not present in the training set has a count of one
        # as a Laplace smoothing factor
        for word in self.token_pos_counts_dict.keys():
            for tag, count in self.tr_pos_counts_dict.items():
                if (word not in self.word_tag_prob[tag]) and \
                   (tag in self.token_pos_counts_dict[word]):
                    self.word_tag_prob[tag][word] = 1

        for tag, words_freq in self.word_tag_prob.items():
            tag_i_sum = sum(self.word_tag_prob[tag].values())
            for word in words_freq.keys():
                self.word_tag_prob[tag][word] /= tag_i_sum
                self.word_tag_prob[tag][word] = np.log2(self.word_tag_prob[tag][word])

    def calc_transition_probs(self):
        """
        """
        # O que acontece no caso de alguma tag não ser prevista?
        # A cadeia de markov não conterá as transições envolvendo-as.
        print("Calculating transition probabilities: part 1.")
        self.tag_markov_model = collections.defaultdict(lambda: collections.defaultdict(int))
        for sentence in tqdm(self.training_set_lst, total=len(self.training_set_lst)):
            for i in range(len(sentence) - 1, 1, -1):
                t_i_2, t_i_1, t_i = sentence[i-2]['pos_tag'], sentence[i-1]['pos_tag'], sentence[i]['pos_tag']
                self.tag_markov_model[t_i_1 + "_" + t_i_2][t_i] += 1

        # dadas as tags de treinamento conhecidas, calcular a probabilidade das transições
        # que não ocorreram no conjunto de treinamento
        print("Calculating transition probabilities: part 2.")
        for sentence in tqdm(self.test_set_lst, total=len(self.test_set_lst)):
            for i in range(len(sentence) - 1, 1, -1):
                t_i_2, t_i_1, t_i = sentence[i-2]['pos_tag'], sentence[i-1]['pos_tag'], sentence[i]['pos_tag']
                two_past_states = t_i_1 + "_" + t_i_2
                if (two_past_states not in self.tag_markov_model) or \
                   (t_i not in self.tag_markov_model[two_past_states]):
                    self.tag_markov_model[two_past_states][t_i] = 1

        for tag_i_1_i_2, tags_j in self.tag_markov_model.items():
            tag_i_1_i_2_sum = sum(tags_j.values())
            for tag_j in tags_j.keys():
                self.tag_markov_model[tag_i_1_i_2][tag_j] /= tag_i_1_i_2_sum
                self.tag_markov_model[tag_i_1_i_2][tag_j] = np.log2(self.tag_markov_model[tag_i_1_i_2][tag_j])

    def viterbi_algotithm(self, sentence):
        """
        """
        T = list(self.tr_pos_counts_dict.keys())
        delta = [np.full((len(T), len(T)), MIN_VALUE) for i in range(len(sentence))]

        k = T.index(sentence[0]['pos_tag'])
        j = T.index(sentence[1]['pos_tag'])
        for l, t_l in enumerate(T):
            delta[0][l][k] = 0

        delta[1][k][j] = 0

        for i in range(2, len(sentence)):
            for j, t_j in enumerate(T):
                w_i_plus_1 = sentence[i]['token']
                # Check if transition may exist. If it does not, we do not need to
                # calculate it
                if w_i_plus_1 not in self.word_tag_prob[t_j]: continue

                log_p_w_i_plus_1 = self.word_tag_prob[t_j][w_i_plus_1]
                for k, t_k in enumerate(T):
                    for l, t_l in enumerate(T):
                        # Check probability of t_l t_k states sequence. If it is
                        # MIN_VALUE this means that delta t_k t_l = 0. So, not need
                        # to calculate it. Also, if t_l t_k -> t_j transition
                        # probability does not exist (P(t_j | t_k t_l) == 0) we do not
                        # need to calculate delta t_j t_k
                        t_k_t_l = t_k + "_" + t_l
                        if (delta[i-1][l][k] == MIN_VALUE) or \
                           (t_j not in self.tag_markov_model[t_k_t_l]): continue

                        log_p_t_j_t_k_t_l = self.tag_markov_model[t_k_t_l][t_j]
                        delta_t_j_t_k_candidate = delta[i-1][l][k] + log_p_t_j_t_k_t_l + log_p_w_i_plus_1

                        if delta_t_j_t_k_candidate > delta[i][k][j]:
                            delta[i][k][j] = delta_t_j_t_k_candidate

        k, j = np.where(delta[-1] == delta[-1].max())
        prob_max_path = delta[-1][k[0]][j[0]]
        X = []
        for i in range(len(sentence) - 1, -1, -1):
            # get sequence of the two last states whose probability is max for the ith tag
            k, j = np.where(delta[i] == delta[i].max())
            t_j = T[j[0]]
            X.insert(0, t_j)

        del T
        del delta

        return X

    def calculate_corpus_viterbi_accuracy(self):
        """
        """
        print("Calculating viterbi algorithm accuracy.")
        total_tags = 0
        total_correct_tags = 0
        for sentence in tqdm(self.test_set_lst, total=len(self.test_set_lst)):
            X = self.viterbi_algotithm(sentence)
            sentence_tags = [word['pos_tag'] for word in sentence]
            total_tags += len(sentence_tags)
            correct_tags = sum([X[i] == sentence_tags[i] for i in range(len(sentence_tags))])
            total_correct_tags += correct_tags

        viterbi_accuracy = total_correct_tags/total_tags

        print("Viterbi algorithm accuracy: ", viterbi_accuracy)
        return viterbi_accuracy

    def free_experiment_variables_memory(self):
        """
        """
        del self.training_set_lst
        del self.test_set_lst
        del self.tr_pos_counts_dict
        del self.tr_token_pos_counts_dict
        del self.word_tag_prob
        del self.tag_markov_model
