# Computational linguistics

[![License](https://img.shields.io/github/license/mashape/apistatus.svg?maxAge=2592000)](https://gitlab.com/jacksonjos/computational_linguistics/blob/master/LICENSE)
All source code in this repository is protected by MIT licence unless indicated otherwise.

This repository contains all work done during the course MAC5725 Linguística Computacional[1].  
These are 2 articles done during the course whose all material produced (article, source code and its environment settings) are in the folders 'exercise_1' and 'exercise_2' and the folder 'MAC0460' with Jupyter notebooks[2] written to learn about RNN concepts necessary to do the work that is in 'exercise_2'.

Both articles have the goal of doing POS tagging[3] over the CETEN Folha Brasilian Portuguese language corpus.  
The first work does that using the Viterbi Algorithm ('exercise_1') and the second one ('exercise_2') uses the BLSTM-CRF network model.

To download the CETEN Folha corpus just go to https://www.linguateca.pt/cetenfolha/index_info.html and fill the form request to receive permission to download it.

[1] https://www.ime.usp.br/index.php?option=com_replicado&task=disciplina&sgldis=MAC5725&Itemid=  
[2] https://jupyter.org/  
[3] https://en.wikipedia.org/wiki/Part-of-speech_tagging
