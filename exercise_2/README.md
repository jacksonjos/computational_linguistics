# Instructions to run the code
The source code of papers work is written in Python and to run the code first is required to install the Python modules and its respective exact versions so that all results obtained might be reproduced.
This readme contains instructions to install the virtual environment and to run the source code of this work.


## Installing environment
To install the virtual environment, use one of the following methods to install Pipenv Python module[1], a module for creating and managing virtual environments using Python language:
### First method
	1. Open the terminal and change to the directory where you'll run the code and type:
	`cd diretory_where_to_run_the_code`
	2. To install Pipenv type:
	`python3 -m pip install pipenv`
### Second method
	1. Open the terminal and change to the directory where you'll run the code and type:
	`cd diretory_where_to_run_the_code`
	1. To install Pipenv type:
	`pip3 install pipenv`
	2. To guarantee that pipenv is in the FILEPATH type:
	`export PATH="$PATH:/home/your_user_name/.local"`
	3. After that you need to make the pipenv command identifiable for in the commando line. So, type:
	`cd /usr/bin`
	`sudo ln -s /home/jackson/.local/bin/pipenv pipenv`
	`source ~/.bashrc`

In case of any doubt check [2].

### Installing Python modules
To install the Python modules using Pipenv using exact the same modules versions used in the experiment do the following:
`pipenv sync`

To open the virtual environment and run the code type: `pipenv shell`
To exit the virtual environment type `exit`.

## How to run the code
The source code of this work contains basically two scripts, a class to do all the operations on the CETENFolha corpus and another one to extract computer hardware configuration, for profiling the operations and to do the tasks of extracting the sentences, calculating the accuracy of the algorithms, and etc.

Considering the flow of the developed work, it is structured in two steps:
1. Is necessary to transform the sentences data structure stored in the sentences.json file created by exercise_1/exercise_1.py script. To do that run the following command:
`python exercise_2.py transform_sentences_to_tuple data_extracted_path/`
2. Is necessary to read the extracted sentences and data structures to train the BLSTM-CRF model using the holdout method of testing. To do that run the following command:
`python exercise_2.py train_BLSTM_CRF_model data_extracted_path/`

**OBS:** 'data_extracted_path/' is the directory where the extracted sentences and data structures will be stored and where are stored the seeds for splitting sentences in training set and test set for reproducibility. Don't change it, unless you change the source code to do it.

[1] https://pipenv.readthedocs.io/en/latest/  
[2] https://stackoverflow.com/questions/14637979/how-to-permanently-set-path-on-linux-unix/14638025#14638025
