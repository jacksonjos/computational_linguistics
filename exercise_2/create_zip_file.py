#!/usr/bin/python

import os
import shutil
import tarfile
# import distutils.core
from distutils import dir_util
from distutils import util

def tardir(path, tar_name):
    with tarfile.open(tar_name, "w:gz") as tar_handle:
        for root, dirs, files in os.walk(path):
            for file in files:
                tar_handle.add(os.path.join(root, file))


if __name__ == '__main__':
    curdir = os.getcwd()
    directory = os.path.join(curdir, "artigo_2_mac_5725_jackson_souza")
    data_directory = os.path.join(directory, 'data')
    if not os.path.exists(directory):
        os.makedirs(directory)
        os.makedirs(data_directory)

    exercise_1 = os.path.join(curdir, "exercise_2.py")
    morphosyntactic_training = os.path.join(curdir, "morphosyntactic_training.py")
    bilstm_config = os.path.join(curdir, "bilstm_config.py")
    bilstm = os.path.join(curdir, "bilstm.py")
    pipfile = os.path.join(curdir, "Pipfile")
    pipfile_lock = os.path.join(curdir, "Pipfile.lock")
    data_seeds = os.path.join(os.path.join(curdir, 'data'), 'seeds.json')
    readme = os.path.join(curdir, "README.md")
    artigo_pdf = os.path.join(curdir, "Part_of_speech_tagging_learning_for_Portuguese_language_on_a_Bidirectional_LSTM_CRF_network_model.pdf")

    files = [exercise_1, morphosyntactic_training, bilstm_config, bilstm, pipfile, pipfile_lock, readme, artigo_pdf]
    for file in files:
        shutil.copy2(file, directory)

    shutil.copy2(data_seeds, data_directory)
    os.system('cp -r profilings artigo_2_mac_5725_jackson_souza')

    tardir('artigo_2_mac_5725_jackson_souza', 'artigo_2_mac_5725_jackson_souza.tar.gz')
