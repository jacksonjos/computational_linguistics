#!/usr/bin/python
import collections
import random
import ujson
import re
import os

from tqdm import tqdm
import numpy as np
import torch
from morphosyntactic_training import MorphosyntacticTraining
from torch.utils.data import DataLoader

START_TAG = "<START>"
STOP_TAG = "<STOP>"

class BLSTMConfig(MorphosyntacticTraining):

    def __init__(self, weight_decay, lr, hidden_dim, epochs, batch_size):
        self.weight_decay = weight_decay
        self.lr = lr
        self.hidden_dim = hidden_dim
        self.batch_size = batch_size
        self.epochs = epochs

    def write_sentences_tuple(self, output_dirpath):
        """
        """
        print("Writing sentences and other data structures.")
        if not os.path.exists(output_dirpath):
            os.makedirs(output_dirpath)

        sentences_tuple_filepath = os.path.join(output_dirpath, 'sentences_tuple.json')
        with open(sentences_tuple_filepath, 'w') as file:
            ujson.dump(self.sentences_tuple, file, indent=4)

    def read_sentences_tuple(self, input_dirpath):
        """
        """
        print("Reading sentences and pos tags and other data structures.")
        sentences_tuple_filepath = os.path.join(input_dirpath, 'sentences_tuple.json')
        with open(sentences_tuple_filepath, 'r') as file:
            self.sentences_tuple = ujson.load(file)

    def build_embeddings_dicts(self, input_dirpath):
        """
        """
        pos_count_filepath = os.path.join(input_dirpath, 'pos_count.json')
        with open(pos_count_filepath, 'r') as file:
            self.pos_counts_dict = ujson.load(file)

        token_pos_count_filepath = os.path.join(input_dirpath, 'token_pos_count.json')
        with open(token_pos_count_filepath, 'r') as file:
            self.token_pos_counts_dict = ujson.load(file)

        self.tag_to_ix = {pos_tag: i for i, pos_tag in enumerate(self.pos_counts_dict)}

        self.tag_to_ix[START_TAG] = len(self.tag_to_ix)
        self.tag_to_ix[STOP_TAG] = len(self.tag_to_ix)

        self.word_to_ix = {word: i for i, word in enumerate(self.token_pos_counts_dict)}

    def transform_sentences_dict_to_tuple(self):
        """
        """
        print("Converting sentences dict to tuple.")

        self.sentences_tuple = []
        for sentence in tqdm(self.sentences):
            sentence_lst = []
            pos_tags_lst = []
            for token_pos_tag_pair in sentence:
                pos_tags_lst.append(token_pos_tag_pair["pos_tag"])
                sentence_lst.append(token_pos_tag_pair["token"])

            sentence_lst = tuple(sentence_lst)
            pos_tags_lst = tuple(pos_tags_lst)
            self.sentences_tuple.append((sentence_lst, pos_tags_lst))

    def filter_sentences(self):
        """
        """
        if not hasattr(self, 'analyzed_sentences'):
            n = len(self.sentences_tuple)
            print("There are {} sentences.".format(n))
            self.analyzed_sentences = [sentence for sentence in self.sentences_tuple
                                        if len(sentence[0]) > 2]
            m = len(self.analyzed_sentences)
            print("There are {} sentences with at least 3 words.".format(m))


            # n = len(self.sentences_pos_tags_lst[0])
            # print("There are {} sentences.".format(n))
            # sentences_tuple = [sentence for sentence in zip(self.sentences_pos_tags_lst[0])
            #                             if len(sentence[0]) > 2]
            # pos_tags_tuple = [pos_tag for pos_tag in zip(self.sentences_pos_tags_lst[0])
            #                             if len(pos_tag[0]) > 2]
            # self.analyzed_sentences = [sentences_tuple, pos_tags_tuple]
            # m = len(self.analyzed_sentences[0])
            # print("There are {} sentences with at least 3 words.".format(m))

    # def separate_corpus(self, seed):
    #     """
    #     """
    #     print("Separating corpus between training and test sets.")
    #     self.filter_sentences()
    #     m = len(self.analyzed_sentences[0])
    #     training_set_size = round(m * 0.8)
    #     random.seed(seed)
    #
    #     random_indexes = np.array(random.sample(range(m), k=training_set_size))
    #     mask = np.zeros(m, dtype=bool)
    #     mask[random_indexes] = True
    #
    #     self.training_set_lst = [self.analyzed_sentences[0][mask],
    #                             self.analyzed_sentences[1][mask]]
    #     self.test_set_lst = [self.analyzed_sentences[0][~mask],
    #                             self.analyzed_sentences[1][~mask]]

    # def build_pos_tag_transition_matrix(self):
    #     """
    #     """
    #     tag_set_cardinality = len(self.pos_counts_dict)
    #     self.pos_tag_transitions = np.zeros((tag_set_cardinality, tag_set_cardinality))
    #
    #     print("Building POS tags transition matrix.")
    #     # MODIFICAR
    #     for _, tag_pos_sentence in tqdm(self.training_set_lst.keys()):
    #         for i in range(len(tag_pos_sentence) - 1):
    #             self.pos_tag_transitions[tag_to_ix[tag_pos_sentence[i+1]]][tag_to_ix[tag_pos_sentence[i]]] += 1

    def get_embedded_word(self, seq):
        """
        """
        idxs = [self.word_to_ix[w] for w in seq]
        return torch.tensor(idxs, dtype=torch.long).cuda()

    def get_embedded_sequence_pos_tags(self, pos_tags):
        """
        """
        idxs = [self.tag_to_ix[w] for w in pos_tags]
        return torch.tensor(idxs, dtype=torch.long).cuda()

    def free_experiment_variables_memory(self):
        """
        """
        del self.training_set_lst
        del self.test_set_lst


class GentleLoader:
    def __init__(self, dataset, batch_size, shuffle):
        self.dataLoader = DataLoader(dataset=dataset,
                                     batch_size=batch_size,
                                     shuffle=True)
    def __iter__(self):
        return ((batch_X, batch_y)
                for (batch_X, batch_y) in self.dataLoader)


class DataHolderGentle():
    """
    Class to store all data using the GentleLoader.

    :param config: hyper params configuration
    :type config: LRConfig or DFNConfig
    :param train_dataset: dataset of training data
    :type train_dataset: torch.utils.data.dataset.TensorDataset
    :param test_dataset: dataset of test data
    :type test_dataset: torch.utils.data.dataset.TensorDataset
    :param valid_dataset: dataset of valid data
    :type valid_dataset: torch.utils.data.dataset.TensorDataset
    :param batch_size: batch size for training
    :type test_batch: batch size for the testing data
    :param test_batch: int
    """
    def __init__(self,
                 config,
                 test_batch=32):
        batch_size = config.batch_size

        config.training_set_lst = [(config.get_embedded_word(sentence), config.get_embedded_sequence_pos_tags(tags))
                                    for sentence, tags in config.training_set_lst if len(sentence) == 20][:512]
        config.test_set_lst = [(config.get_embedded_word(sentence), config.get_embedded_sequence_pos_tags(tags))
                                    for sentence, tags in config.test_set_lst if len(sentence) == 20][:256]

        self.train_loader = GentleLoader(dataset=config.training_set_lst,
                                         batch_size=batch_size,
                                         shuffle=True)
        self.test_loader = GentleLoader(dataset=config.test_set_lst,
                                        batch_size=batch_size,
                                        shuffle=True)
