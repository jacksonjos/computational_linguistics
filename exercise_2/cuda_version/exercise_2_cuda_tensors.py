#!/usr/bin/python
import sys
import os
import cProfile
import statistics
import ujson

import cpuinfo
import psutil
import platform
import math
import random
from bilstm import BiLSTM_CRF
from bilstm_config import BLSTMConfig, DataHolderGentle

from tqdm import tqdm
import torch
import torch.optim as optim
from sklearn.metrics import accuracy_score, f1_score

device = torch.device("cuda:0")

def get_seeds(sentences_statistics_dirpath):
    """
    """
    seeds_filepath = os.path.join(sentences_statistics_dirpath, "seeds.json")
    if not os.path.exists(seeds_filepath):
        seeds = [random.randint(0, 1000) for i in range(10)]
        with open(seeds_filepath, 'w') as file:
            ujson.dump(seeds, file, indent=4)
    else:
        with open(seeds_filepath, 'r') as file:
            seeds = ujson.load(file)

    return seeds


def update_model_measures(sentences_statistics_dirpath, accuracy, f1_measure, measures_index):
    """
    """
    model_measures_filepath = os.path.join(sentences_statistics_dirpath, "model_measures.json")
    if (not os.path.exists(model_measures_filepath)) or (measures_index == 0):
        model_measures = [(accuracy, f1_measure)]
        with open(model_measures_filepath, 'w') as file:
            ujson.dump(model_measures, file, indent=4)
    else:
        with open(model_measures_filepath, 'r') as file:
            model_measures = ujson.load(file)

        model_measures.append((accuracy, f1_measure))
        with open(model_measures_filepath, 'w') as file:
            ujson.dump(model_measures, file, indent=4)

    return model_measures


def train_model(blstm_config, pos_tag_data):
    """
    """
    model = BiLSTM_CRF(blstm_config).cuda()
    optimizer = optim.SGD(model.parameters(), lr=blstm_config.lr,
                          weight_decay=blstm_config.weight_decay, momentum=0.1)
    for epoch in range(blstm_config.epochs):
        print("Running {} training epoch of 3.".format(epoch))
        for sentence_in, targets in tqdm(pos_tag_data.train_loader):
            # Step 1. Remember that Pytorch accumulates gradients.
            # We need to clear them out before each instance
            model.zero_grad()
            #
            # # Step 2. Run our forward pass.
            loss = model.neg_log_likelihood(sentence_in, targets)
            #
            # # Step 3. Compute the loss, gradients, and update the parameters by
            loss.backward()
            optimizer.step()


    return model


def calc_accuracy(blstm_config, model, pos_tag_data):
    """
    """
    print("Calculating model accuracy prediction over test set.")
    true_tags_lst = []
    tags_predicted_lst = []
    for precheck_sent, embedded_tags in tqdm(pos_tag_data.test_loader):
        _, embedded_tag_seq = model(precheck_sent)
        true_tags_lst.extend(list(embedded_tags.view(-1)))
        tags_predicted_lst.extend(sum(embedded_tag_seq, []))

    accuracy = accuracy_score(true_tags_lst, tags_predicted_lst)
    f1_measure = f1_score(true_tags_lst, tags_predicted_lst, average='macro')
    print(accuracy, f1_measure)
    return accuracy, f1_measure


def pos_tagging_classification(blstm_config, seeds, i):
    """
    """
    blstm_config.separate_corpus(seeds[i])
    pos_tag_data = DataHolderGentle(blstm_config)
    # blstm_config.build_pos_tag_transition_matrix()
    model = train_model(blstm_config, pos_tag_data)
    accuracy, f1_measure = calc_accuracy(blstm_config, model, pos_tag_data)
    blstm_config.free_experiment_variables_memory()

    return accuracy, f1_measure


def run_pos_tagging_classification_experiment(sentences_statistics_dirpath):
    """
    """
    blstm_config = BLSTMConfig(1e-4, 0.1, 4, 3, 32)
    blstm_config.read_sentences_tuple(sentences_statistics_dirpath)
    blstm_config.build_embeddings_dicts(sentences_statistics_dirpath)
    seeds = get_seeds(sentences_statistics_dirpath)

    accuracy, f1_measure = pos_tagging_classification(blstm_config, seeds, 0)
    model_measures = update_model_measures(sentences_statistics_dirpath, accuracy, f1_measure, 0)
    # for i in range(len(seeds)):
    #     accuracy, f1_measure = pos_tagging_classification(blstm_config, seeds, i)
    #     model_measures = update_model_measures(sentences_statistics_dirpath, accuracy, f1_measure, i)

    #accuracy_lst, f1_measure_lst = zip(*model_measures)
    #mean_accuracy = statistics.mean(accuracy_lst)
    #stdev_accuracy = statistics.stdev(accuracy_lst)
    #min_accuracy = min(accuracy_lst)
    #max_accuracy = max(accuracy_lst)

    #print("The mean accuracy is: ", mean_accuracy)
    #print("The standard deviation is: ", stdev_accuracy)
    #print("The min accuracy is: ", min_accuracy)
    #print("The max accuracy is: ", max_accuracy)

    #mean_f1_measure = statistics.mean(f1_measure_lst)
    #stdev_f1_measure = statistics.stdev(f1_measure_lst)
    #min_f1_measure = min(f1_measure_lst)
    #max_f1_measure = max(f1_measure_lst)

    #print("The mean f1_measure is: ", mean_f1_measure)
    #print("The standard deviation is: ", stdev_f1_measure)
    #print("The min f1_measure is: ", min_f1_measure)
    #print("The max f1_measure is: ", max_f1_measure)

def convert_sentences(sentences_statistics_dirpath):
    """
    """
    blstm_config = BLSTMConfig()
    blstm_config.read_sentences_and_statistics(sentences_statistics_dirpath)
    blstm_config.transform_sentences_dict_to_tuple()
    blstm_config.write_sentences_tuple(sentences_statistics_dirpath)


def profile_activity_execution(func, step_name, *args):
    """
    """
    profiler = cProfile.Profile()
    profiler.runcall(func, *args)
    profiler.dump_stats(step_name + '.prof')


def write_system_info(step_name):
    """
    """
    file = open(step_name + "_machine_config.txt", 'w')

    info = cpuinfo.get_cpu_info()
    file.write("Processors: {}\n".format(info['brand']))
    file.write("Number of cores: {}\n".format(info['count']))
    file.write("L1 cache memory size: {}\n".format(info['l1_data_cache_size']))
    file.write("L2 cache memory size: {}\n".format(info['l2_cache_size']))
    file.write("L3 cache memory size: {}\n".format(info['l3_cache_size']))

    memory_size = round(psutil.virtual_memory().total / math.pow(2, 30))
    file.write("\nPhysical memory size: {} GB\n".format(memory_size))

    os_name = platform.system()
    if os_name == 'Linux':
        os_version = " ".join([s.capitalize() for s in platform.linux_distribution()])
        file.write("Operating system: {} {}\n".format(os_name, os_version.strip()))
        file.write("Kernel: {}\n".format(platform.release()))
    else:
        file.write("Operating system: {} {}".format(os_name, platform.release()))

    file.close()


if __name__ == '__main__':
    if sys.argv[1] == 'transform_sentences_to_tuple':
        profile_activity_execution(convert_sentences, *sys.argv[1:])
    if sys.argv[1] == 'train_BLSTM_CRF_model':
        profile_activity_execution(run_pos_tagging_classification_experiment, *sys.argv[1:])
    elif sys.argv[1] == 'tune_hyperparameters':
        profile_activity_execution(tune_model_hyperparameters, *sys.argv[1:])

    write_system_info(sys.argv[1])
