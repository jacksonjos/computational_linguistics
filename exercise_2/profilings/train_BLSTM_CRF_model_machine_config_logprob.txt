Processors: Intel(R) Core(TM) i7 CPU       X 990  @ 3.47GHz
Number of cores: 12
L1 cache memory size: 32 KB
L2 cache memory size: 256 KB
L3 cache memory size: 12288 KB

Physical memory size: 47 GB
Operating system: Linux Ubuntu 18.04 Bionic
Kernel: 4.15.0-39-generic
